/*Experiment2
There are FIVE students in a class. Develop a Java program that will output roll no, total marks and
average marks of each student. Use object-oriented methodology.*/

 class student
{
	private int rollno;
	private int mark1;
	private int mark2;
	private int mark3;
	public student(int r, int m1, int m2, int m3)
	{
		rollno=r;
		mark1=m1;
		mark2=m2;
		mark3=m3;
	}
	public int getRollNo()
	{
		return rollno;
	}
	public int total()
	{
		return mark1+mark2+mark3;
	}
	public int avarege()
	{
		return (mark1+mark2+mark3)/3;
	}
}
public class Experiment2
{
	public static void main(String args[])
	{
		try {
		student[] s=new student[5];
		s[0]=new student(1,20,18,14);
		s[1]=new student(2,18,16,12);
		s[2]=new student(3,15,17,19);

		for(int i=0;i<5;i++)
		{
			System.out.println("rollno"+s[i].getRollNo()+" ");
			System.out.println("total"+s[i].total()+" ");
			System.out.println("avarege"+s[i].avarege()+" ");
		}
	}
	catch (Exception e)
	{
		System.out.println("Error occured");
	}
	}
}