class first
{
	synchronized public void display(String msg) //synchronous using keyword/method which means nes at a time
	{
			System.out.print("["+msg);
			try
			{
				Thread.sleep(1000);

			}
			catch (InterruptedException e)
			{
				System.out.println(e);
			}
			System.out.print("]");
	}
}
class second extends Thread 
{
		String msg;
		first fobj;
		second(first fp,String str)
		{
			fobj=fp;
			msg=str;
			start();
		}
		public void run()
		{
			fobj.display(msg);
		}
}
public class synchronousM
{
	public static void main(String args[])
	{
		first fnew = new first();
		second s1 = new second(fnew,"welcome");
		second s2 = new second(fnew,"new");
		second s3 = new second(fnew,"user");
	}
}