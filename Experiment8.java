/*Develop a GUI based Java program to display Fibonacci numbers up to a limit entered using
TextField. Use appropriate components and event handling.*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

class Fibonacci extends JFrame 
{
	private JTextField tx1;
	private JTextField tx2;

	private JLabel lb1,lb2;
	private JButton b;
    public Fibonacci()

	{
		tx1 = new JTextField(10);
		tx2 = new JTextField(100);

		lb1 = new JLabel("Number");
		lb2 = new JLabel("Fibonacci series");

		b = new JButton("print");

		setSize(1500,500);
		setLayout(new FlowLayout());
		add(lb1);
		add(tx1);
		add(lb2);
		add(tx2);
		add(b);
		b.addActionListener(new EventHandler());

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

class EventHandler implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		String str1=tx1.getText();
		int n =Integer.parseInt(str1);
		int t1=0;
		int t2=1;
		int t3;
		String str2 = Integer.toString(t1);
		tx2.setText(tx2.getText()+str2+"\t");
		String str3 = Integer.toString(t2);
		tx2.setText(tx2.getText()+str3+"\t");
		String str4;
		while(n>0)
		{
			t3=t1+t2;
			t1=t2;
			t2=t3;
			str4=Integer.toString(t3);
			tx2.setText(tx2.getText()+str4+"\t");
			n--;
		}

	}
}
}


public class Experiment8
{
	public static void main(String args[])
	{
		Fibonacci f =new Fibonacci();

	}
}