//binary search

public class Experiment5B
{
	public static void main(String args[])
	{
		int arr[]= {0,5,9,12,14,60};
		int key = 14;
		int first =0;
		int last = arr.length-1;
		int mid =(first+last)/2;

		while(first<=last)
		{
			
			if(arr[mid]<key)
			{
				first=mid+1;
			}
			else if(arr[mid]==key)
			{
				System.out.println("key found at "+mid);
				break;
			}
			else 
			{
				last=mid-1;
			}
			mid=(first+last)/2;
		}
		
		if(first>last)
		{
			System.out.println("key not found");
		}
	}
}