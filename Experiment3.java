/*Experiment3
Develop a Java program that can perform the following operations
1. multiply a complex number by another complex no
2. multiply a complex number by a scalar*/




public class ComplexNumber
 {
 	private int real;
 	private int img;

 	public ComplexNumber(int real,int img)
 	{
 		this.real=real;
 		this.img=img;
 	}
 	public ComplexNumber multiply(ComplexNumber c)
 	{
 		int real = this.real*c.real-this.img*c.img;
 		int img=this.real*c.img+this.img*c.real;
 		ComplexNumber result= new ComplexNumber(real,img);
 		return result;
 	}
 	public ComplexNumber multiply(int c)
 	{
 		int real=this.real*c;
 		int img=this.img*c;
 		ComplexNumber result=new ComplexNumber(real,img);
 		return result;
 	}
 	public void print()
 	{
 		System.out.println(real+"+"+img+"i");
 	}
 	
 }


public class Experiment3
{
	public static void main (String args[])
	{
		ComplexNumber c1 = new ComplexNumber(2,3);
		ComplexNumber c2=new ComplexNumber(4,6);
		ComplexNumber c3=c1.multiply(c2);
		ComplexNumber c4=c1.multiply(3);
		c3.print();
		c4.print();
	}
}