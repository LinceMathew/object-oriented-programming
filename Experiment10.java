/*Develop GUI based Java program that allows to input velocity in Kilometres/Hour, convert and
display in Meters/Second. Use appropriate components and event handling.*/
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class converter extends JFrame
{
   public JLabel converter;
   public JLabel lb1;
   public JLabel lb2;
   public JTextField km;
   public JTextField m;
   public JButton b;

   public converter()
   {
   	converter = new JLabel("speed converter");
   	lb1 = new JLabel("kilometer/hour");
   	km= new JTextField(10);
   	lb2 = new JLabel("meter/second");
   	m= new JTextField(10);
   	b= new JButton("converter");
   	setSize(1000,500);
   	setLayout(new FlowLayout());
   	add(converter);
   	add(lb1);
   	add(km);
   	add(lb2);
   	add(m);
   	add(b);
   	b.addActionListener(new EventHandler() );
   	setDefaultCloseOperation(EXIT_ON_CLOSE);
   	setVisible(true);
   }
   	class EventHandler implements ActionListener
   	{
   		public void actionPerformed(ActionEvent e)
   		{
   			String str =km.getText();
   			float result = Float.parseFloat(str)*5/18;
   			String str2 = Float.toString(result);
   			m.setText(str2);
   		}
   	}  
}
public class Experiment10
{
	public static void main(String args[])
	{
		converter c = new converter();
	}
}