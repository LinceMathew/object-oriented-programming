public class StringExample
{
	public static void main(String ags[])
	{
		try {
			int value= 40;
		char ch[]={'a','b','c','d'};
		char c='a';
		byte b[]={64,67,65,70}; //asci
		String s3="abcd"; //using java literals
		String s= new String("abcd"); //using new keyword
		String s1= new String("LIVEI   III"); //using char array to string
		String s4=new String(ch,2,2); //(array of characters,starting index,int count)
		String s5= new String(b,1,2);
		System.out.println(s);
		System.out.println(s1);
		System.out.println(s3);
		System.out.println(s4);
 		System.out.println(s5);
 		System.out.println(s3.length()); //length of the String
 		System.out.println(s3.equals(s1)); //compare to String
 		System.out.println(s3.equalsIgnoreCase(s1));//compare two string by avoiding case
        System.out.println(s4==s3);
        System.out.println(s3.compareTo(s1));//returns an integer value that describs if 
                                             //first string is less than,equal,or greater than

        if(s3.contains("b"))
        {
        	System.out.println("it contains");
        }
        else
        System.out.println(s3.contains("v")); //searching  in String
       
    	 System.out.println(s3.charAt(3)); //return character of the string at given index
    	 
    	 String new_s1=s1.replace('I','O');
    	 String new_s6=s1.replaceAll("I","O");
    	 System.out.println(new_s1+new_s6);

    	 String s8 = String.valueOf(value);
    	 System.out.println(s8+10); //concatenating string with 10
    	 boolean bol = true;
    	 String s9 = String.valueOf(bol);
    	 System.out.println(s9);
    	 String s10=String.valueOf(c);
    	 System.out.println(s10);

    	 StringExample x= new StringExample();
    	 String s11 = String.valueOf(x);
    	 System.out.println(s11);
        }

        

        catch (Exception e)
        {
        	System.out.println("error occured  :"+e);
        }

		
	}
}