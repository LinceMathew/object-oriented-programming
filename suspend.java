class ThreadA extends Thread
{
	public void run()
		{
		try{
				System.out.println(Thread.currentThread().getName());
			
				Thread.sleep(500);
				
			}
			
		
		catch(InterruptedException e)
		{
			System.out.println("Error");
		}
	}
}
 
class suspend
{
 public static void main(String args[])
 {
 		ThreadA t1= new ThreadA();
 		ThreadA t2= new ThreadA();
 		ThreadA t3= new ThreadA();
 		
 		t1.start();
 		t2.start();
 		t2.suspend();//suspend t2 thread
 		System.out.println("thread1 suspended\n");
 		t3.start();
 		t2.resume();
 		System.out.println("thread1 resumed\n");
 		t1.stop();
 		System.out.println("thread0 stopped");
 }

}