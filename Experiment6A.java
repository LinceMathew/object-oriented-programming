//selectionSort

import java.util.Scanner;
public class Experiment6A
{
	public static void main(String args[])
	{
      Scanner s = new  Scanner(System.in);
      System.out.println("enter number of elements");
      int n= s.nextInt();
      int arr[] = new int[n];
      System.out.println("enter elements");
      int swap;
      for(int i=0;i<n;i++)
      {
      	 arr[i]=s.nextInt();
      }
      for(int i=0;i<n-1;i++)
      {
      	int min=i;
      	for(int j=i+1;j<n;j++)
      	{
      		if(arr[j]<arr[min])
      		{
      			min=j;
      		}
      	}
      	swap = arr[i];
      	arr[i]=arr[min];
      	arr[min]=swap;

      }
      System.out.println("sorted array");
      for(int i=0;i<n;i++)
      {
      	System.out.println(arr[i]);
      }
      
	}
}