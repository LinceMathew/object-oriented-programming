/*Experiment4Create a class BankAccount having the following 
specificationattributes-account no, balance
methods -credit, debit, print
Create another class SavingsAccount through inheritance.
It should add the functionality for computing the interest*/
	

	class BankAccount
	{
		protected int AccountNo;
		protected int Balance;
		public BankAccount(int AccountNo,int Balance)
		{
			this.AccountNo=AccountNo;
			this.Balance= Balance;
		}
		public  void print()
		{
			System.out.println("Available Balance"+Balance);
			System.out.println("AccountNumber"+AccountNo);
		}


		public int credit(int amount)
		{
				System.out.println(amount+"credited in your account");
				Balance= Balance+amount;
				print();
				return Balance;
		}
		public int debit(int amount)
		{
			System.out.println(amount+"debited from your account");
			Balance=Balance-amount;
			print();
			return Balance;
		}
	}
	class savingsAccount extends BankAccount
	{
		private  final static int rate = 2;
		public  savingsAccount(int AccountNo,int Balance)
		{
			super(AccountNo,Balance);
		}
		public void SimpleInterest(int balance)
		{
		  int si = Balance* rate/100;
		  System.out.println("Simple interest for available balance"+balance+"is"+si);
		}
	}
	public class  Experiment4
{
	public static void main(String args[])
	{
		savingsAccount s = new savingsAccount(1634,2000);

		s.print();
		int balance1 = s.credit(500);
		int balance2 = s.debit(200);
		s.SimpleInterest(balance1);
		s.SimpleInterest(balance2);

	}
}