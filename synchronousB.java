class first
{
	public void display(String msg)
	{
			System.out.print("["+msg);
			try
			{
				Thread.sleep(2000);

			}
			catch (InterruptedException e)
			{
				System.out.println(e);
			}
			System.out.println("]");
	}
}
class second extends Thread 
{
		String msg;
		first fobj;
		second(first fp,String str)
		{
			fobj=fp;
			msg=str;
			start();
		}
		public void run()
		{
			synchronized (fobj)
			{
				fobj.display(msg);
			}
			
		}
}
public class synchronousB
{
	public static void main(String args[])
	{
		first fnew = new first();
		second s1 = new second(fnew,"welcome");
		second s2 = new second(fnew,"new");
		second s3 = new second(fnew,"user");
	}
}