import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFile
{
	public static void main(String Args[])
	{
		try{
			File myobj= new File("/home/lince/Documents/java progam/firstfile.txt");
			Scanner myReader = new Scanner(myobj);
			while(myReader.hasNextLine())
			{
				String data = myReader.nextLine();
				System.out.println(data);
			}
			myReader.close();
		}
		catch( FileNotFoundException e){
			System.out.println("error occured");
			e.printStackTrace();
		}

	}
}