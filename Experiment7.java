/*Experiment7
Implement CircularQueue. It must throw exception under the following conditions.
1. enqueue an item while the queue is full
2. dequeue an item while the queue is empty.*/


import java.util.Scanner;


 class CircularQueue
{
	private int [] queue;
	private int size;
	private  int front;
	private int rear;

	public   CircularQueue(int size)
	{
		queue = new int[size];
		this.size=size;
		front = -1;
		rear = -1;
	}
	public boolean isFull()
	{
		if(((front== 0)&&(rear==size-1))||(front ==(rear+1)%size))

			return true;
		else 
			return false;
	}
	public boolean isEmpty()
	{
		if(front==-1)
			return true;
		else 
			return false;
	}
	public void enqueue(int item) throws QueueException
	{
		if(isFull())
		{
			throw new QueueException("Queue overflow");
		}
		else
		{
			if(front==-1)
				front =0;
			rear = (rear+1)%size;
			queue[rear]=item;
		}
	}
	public int  dequeue() throws QueueException
	{
		int item;
		if(isEmpty())
		{
			throw new QueueException("Queue underflow");

		}
		else
		{
			
			item = queue[front];
			if(front==rear)
			{
				front = rear = 0;
			}
			else
				front = (front+1)%size;
		}
		return item;

		}
public void display() throws QueueException
{
	int i;
	if(isEmpty())
		throw new QueueException("queue underflow");
	else
	{
		if (front<=rear)
		 {

			for(i=front;i<=rear;i++)
			{
				System.out.println(queue[i]);
			}
		}
		else
		{
			for(i=0;i<=rear;i++)
			{
				System.out.println(queue[i]);
			}
			for(i=front;i<size;i++)
			{
				System.out.println(queue[i]);
			}
		}
	}

}

}

 class QueueException extends Exception
{
	public QueueException(String q)
	{
		super(q);
	}
}




public class Experiment7
{
 public static void main(String args[])
 {
 	Scanner s =new Scanner(System.in);
 	int option;
 	System.out.println("enter the size");

 	int size = s.nextInt();
 	CircularQueue q = new CircularQueue(size); 

 	do
 	{
 		System.out.println("enter choice... 1.enqueue..2.dequeue..3.display... 4.exit");
 		option=s.nextInt();
 		try{
 			switch(option)
 			{
 				case 1 : System.out.println("enter item");
 						int item =s.nextInt();
 						q.enqueue(item);
 						break;
 				case 2 	: int del= q.dequeue();
 							System.out.println("deleted element"+del);
 							break;

 				case 3 : q.display();
 				         break;
 				 case 4:break;


 				 default:System.out.println("invalid option");        
 			}
 		}
 		catch( QueueException e)
 		{
 			System.out.println(e);
 		}
 	}while(option!=0);
 }
}