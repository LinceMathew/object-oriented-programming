/*Experiment1
Develop a Java program to find the type of roots of a quadratic equation. Use object-oriented
methodology.
For example - output should be "real-identical", "real-distinct", "imaginary"
*/


 class Quadracticequation
{
	private int a;
	private int b;
	private int c;

	public Quadracticequation(int x,int y,int z)
	{
		a=x;
		b=y;
		c=z;
	}
	public void printTypeOfRoots()
	{
		int d=b*b-4*a*c;
		if(d==0)
		{
			System.out.println("real and identical");

		}
		if(d>0)
		{
			System.out.println("real and distinct");
		}
		if(d<0)
		{
			System.out.println("imaginary roots");
		}
	}
}



public class problem1
{
	public static void main(String args[])
	{
		Quadracticequation q= new Quadracticequation(2,5,3);
		q.printTypeOfRoots();
	}
}