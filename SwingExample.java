import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class SwingExample extends JFrame
{
	public SwingExample()
	{
		setTitle("new window");
		setSize(300,300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

	}
	public static void main(String args[])
	{
		SwingExample se = new SwingExample();
		se.setVisible(true);
	}
}