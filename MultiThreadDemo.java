
 class ThreadA extends Thread
	{
		public void run()
		{
			try{
			for(int i=0;i<11;i++)
			{
				System.out.println("i"+i);
				Thread.sleep(500);
				
			}
			System.out.println("threadA");
		}
		catch(InterruptedException e)
		{
			System.out.println("Error");
		}
		}
	}
 class ThreadB extends Thread
{
	public void run()
	{
		for(int j=20;j<31;j++)
		{
			System.out.println("j"+j);

		}
		System.out.println("threadB");
	}
}
class ThreadC extends Thread{
	public void run()
	{
		for(int k=30;k<41;k++)
		{
			System.out.println("k"+k);
		}
		System.out.println("threadC");
	}
}
class MultiThreadDemo
{
 public static void main(String args[])
 {
 		ThreadA t1= new ThreadA();
 		ThreadB t2 = new ThreadB();
 		ThreadC t3 = new ThreadC();
 		t1.start();
 		t2.start();
 		t3.start();
 }

}