/*Write a GUI based Java program to check whether a given number is prime or not. Use appropriate
components and event handling.*/
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class prime extends JFrame
{
   public JLabel number;
   public JTextField primeornot;
   public JTextField numberText;
   public JButton b;

   public prime()
   {
   	number =new JLabel("enter the number");
   	primeornot = new JTextField(10);
   	numberText = new JTextField(10);
   	b= new JButton("check prime");

   	setSize(1000,500);
   	setLayout(new FlowLayout());
   	add(number);
   	add(numberText);
   	add(primeornot);
   	add(b);
   	b.addActionListener(new EventHandler());
   	setDefaultCloseOperation(EXIT_ON_CLOSE);
   	setVisible(true);
   }
   class EventHandler implements ActionListener
   {
   	public void actionPerformed(ActionEvent e)
   	{
   		String str =numberText.getText();
   		int n = Integer.parseInt(str);
   		int i;
   		for(i=2;i<=n/2;i++)
   		{
   			if(n%i==0)
   			{
   					break;
   			}
   		}
   		if(i>n/2)
   		
   			primeornot.setText(str+" is prime");
   		
   		else
   		
   			primeornot.setText(str+" not prime");
   		
   	}
   }
}
public class Experiment9
{
	public static void main(String args[])
	{

	prime p =new prime();
}
}